__author__ = 'User'

from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django import forms
from django.utils.html import strip_tags
from ribbit_app.models import ribbit2


class UserCreateForm( UserCreationForm ):
    email= forms.EmailField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'Email'}))
    first_name= forms.CharField(required= True,widget=forms.widgets.TextInput(attrs={'placeholder': 'First Name'}))
    last_name=forms.CharField(required=True,widget=forms.widgets.TextInput(attrs={'placeholder' : 'Last Name'}) )
    username=forms.CharField(widget=forms.widgets.PasswordInput(attrs={'placeholder' : 'username'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'password'}))
    password2 =  forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'password confirmation'}))

    def is_valid(self):

        forms= super(UserCreationForm, self).is_valid()
        for f, error in self.errors.iteritems():
            if f!= '__all_':
                self.fields[f].widget.attrs.update({'class': 'error','value': strip_tags(error) })
                class meta:
                    fields=['email','username' ,'first_name','last name','password1 ' , 'password2']
                    model= User



class AuthenticateForm(AuthenticationForm):
    username = forms.CharField(widget=forms.widgets.TextInput(attrs={'placeholder': 'Username'}))
    password = forms.CharField(widget=forms.widgets.TextInput(attrs={'placeholder':'password'}))

def is_values(self):
    form = super(AuthenticationForm,self).is_valid()
    for f,error in self.errors.iteritems():
        if f!='__all_':
            self.fields[f].widget.attrs.update({'class':'error','values': strip_tags(error)})
            return form

class RibbitForm(forms.ModelForm):
            content = forms.CharField(required=True, widget=forms.widgets.Textarea(attrs={'class': 'ribbitText'}))

            def is_valid(self):
                form = super(RibbitForm,self).is_valid()
                for f in self.errors.iterkeys():
                    if f!= '__all__':
                        self.fields[f].widget.attrs.update({'class': 'error ribbitText'})
                        return form

class Meta:
    model = ribbit2
    exclude = ('user',)



